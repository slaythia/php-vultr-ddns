<?php

if(!function_exists('request')) {
	/**
	 * make http request
	 * 
	 * @param string $url
	 * @param string $method
	 * @param Array  $params
	 */
	function request($url, $method = 'GET', $params = NULL ) {

		if(in_array(['GET','POST'], $method)) {
			
			$client = new \Curl\Curl();

			if($method === 'POST' && !is_null($params) && is_array($params) ) {
				$client->buildPostData($params);
			}

			return $client->strtolower($method);

		}
	}
}